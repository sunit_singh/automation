
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Cell;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Locale;
import java.time.YearMonth;
import java.text.SimpleDateFormat;
import java.util.Date;


import java.io.FileOutputStream;
import java.util.Collection;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

class ClientCases {

    public static void main(String[] a) 
	{
		
		String connection_det = null;
		String client_det = "XYZ";
		List ccd = new ArrayList();
		
        try 
		{
            Workbook wb = WorkbookFactory.create(new FileInputStream(new File("C:/Mail/client_db_details.xls")));
            Sheet mySheet = wb.getSheetAt(0);
            Iterator<Row> rowIter = mySheet.rowIterator();
			
			
			
			  while (rowIter.hasNext()) 
			  {

                Row currentRow = rowIter.next();
                Iterator<Cell> cellIterator = currentRow.cellIterator();;
				
				List celldata = new ArrayList();

                while (cellIterator.hasNext()) 
				{
					
					Cell currentCell = cellIterator.next();
			
			          switch (currentCell.getCellType()) 
					  {
						  	  
						case 1:
						
						if ( connection_det == null) {
						connection_det = currentCell.getStringCellValue();
						}
						/**System.out.print(connection_det);*/
						
						
						
						if ( client_det == null ) {
						client_det = currentCell.getStringCellValue();
						celldata.add(currentCell.getStringCellValue());
						}
						
						break;
                } 
				
				client_det = null;
			      
				}
				
				
				System.out.println(connection_det);
				
				if ( connection_det != null ) {
					
					OracleConnection oc = new OracleConnection();
				  
				    int a1 = oc.getCount(connection_det);
					System.out.println(a1);
					celldata.add(a1);
			  }
			  
			  ccd.add(celldata);
			  connection_det = null;
			  client_det = "XYZ";
		 
			  }
			  
			  int curr_year1;
		
			  String excelFilePath = "C:/Mail/Client_Count_details/Client_Case_Details";
			  SimpleDateFormat mon1 = new SimpleDateFormat("MMM");
			  SimpleDateFormat yy1 = new SimpleDateFormat("YYYY");
		
			  String currentmonth1 = mon1.format(new Date());
			  String currentyear1 = yy1.format(new Date());
			  
			  System.out.println(currentmonth1);
		
			  int curr_year = Integer.parseInt(currentyear1);
		
			  if ( currentmonth1.equals("Jan") ) //Need to change month, to Jan
			  {
				  curr_year1 = curr_year - 1;
		      }
			  else if ( currentmonth1.equals("Feb") ) //Need to change month, to Feb
			 {
				  curr_year1 = curr_year; //Need to remove +1.
				  System.out.println(curr_year1);
		     }
			  else 
			 {
			      curr_year1 = curr_year;
			 }
		
			String year_path = curr_year1 + ".xls";
			String final_excel_path = excelFilePath + year_path;
			 
			  CreateExcelFile ce = new CreateExcelFile();
				  
				  ce.WriteExcel(ccd, final_excel_path);
				  
			  ReadPrintTotal rp = new ReadPrintTotal();
			   
			     rp.ReadWriteTotal(final_excel_path);
				 
			  ReadPrintColumnTotal rc = new ReadPrintColumnTotal();

				 int a2 = rc.ReadWriteColumnTotal(final_excel_path);
				 
			  SendEmail se = new SendEmail();
			   
			     se.SendMailFile(final_excel_path,a2);
		
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


class OracleConnection
{  

public int getCount(String connectstring)
{
try
{  
//step1 load the driver class  
    Class.forName("oracle.jdbc.driver.OracleDriver");  
  
//step2 create  the connection object  

    String str[] = connectstring.split(",");
			
        Connection con = DriverManager.getConnection(str[0],str[1],str[2]);
  
//step3 create the statement object  
        Statement stmt=con.createStatement();  
  
//step4 execute query  
        ResultSet rs=stmt.executeQuery("select count(1) from case_master c where c.case_id > 0 and to_char(trunc(c.create_time), 'YYYY') = case when to_char(trunc(sysdate), 'MON') = 'JAN' then to_char(trunc(sysdate), 'YYYY') - 1 when to_char(trunc(sysdate), 'MON') = 'FEB' then to_number(to_char(trunc(sysdate), 'YYYY')) else to_number(to_char(trunc(sysdate), 'YYYY')) end and to_char(trunc(c.create_time), 'MON') = case when to_char(trunc(sysdate), 'MON') = 'JAN' then to_char(add_months(trunc(sysdate), -1), 'MON') when to_char(trunc(sysdate), 'MON') = 'FEB' then to_char(add_months(trunc(sysdate), -1), 'MON') else to_char(add_months(trunc(sysdate), -1), 'MON') end");
  
        while(rs.next())  {

			int b = rs.getInt(1);
 
//step5 close the connection object  
			con.close();
				return b;
            }
 
  
        }  
catch(Exception e){ System.out.println(e);}  
return -1;
    }  
}

//--------------code for writing data into excel----------------------------
class CreateExcelFile {
  int rownum = 0;
  HSSFSheet firstSheet;
  Collection<File> files;
  HSSFWorkbook workbook;
  File exactFile;
  
  //String excelFilePath = "ExcelSheet.xls";
  
  public void WriteExcel(List<List> l1, String final_excel_path) throws Exception {
    try {
		
		//FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		//Workbook workbook = WorkbookFactory.create(inputStream);
		
		File f = new File(final_excel_path);
		boolean bool = false;
		bool = f.exists();
		if ( bool == false ) {
		f.createNewFile();	
		workbook = new HSSFWorkbook();
		firstSheet = workbook.createSheet("Client Cases Count");
		}
		else 
		{
			//FileInputStream ExcelFileToRead = new FileInputStream("C:/Users/ssingh3/ExcelSheet.xls");
			FileInputStream ExcelFileToRead = new FileInputStream(final_excel_path);
			workbook = new HSSFWorkbook(ExcelFileToRead); 
		}	
		
		CellStyle set_style = workbook.createCellStyle();
		Font set_font = workbook.createFont();
		set_font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		set_style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());//added this code on 26-10-2017
		set_style.setFillPattern(CellStyle.SOLID_FOREGROUND);//added this code on 26-10-2017
		set_style.setFont(set_font);
		
		CellStyle set_style5 = workbook.createCellStyle();//added this code for giving colours to text
		Font set_font_color = workbook.createFont();//added this code for giving colours to text
		set_font_color.setBoldweight(Font.BOLDWEIGHT_BOLD);//added this code for giving colours to text
		set_font_color.setColor(IndexedColors.BLUE.getIndex());//added this code for giving colours to text
		set_style5.setFillForegroundColor(IndexedColors.ORANGE.getIndex());//added this code on 26-10-2017
		set_style5.setFillPattern(CellStyle.SOLID_FOREGROUND);//added this code on 26-10-2017
		set_style5.setFont(set_font_color);//added this code for giving colours to text
		
		Sheet firstSheet = workbook.getSheetAt(0);
		
		int i = 0;
		Cell cell1 = null;
		Row row = null;
		
		int lastrow1 = firstSheet.getLastRowNum();
		
			row = firstSheet.getRow(6);//CHANGED FROM 0 TO 6
			if ( row != null ) 
			{
				System.out.println("Row is present");
			}
			else 
			{
				row = firstSheet.createRow(6);
				Cell cell = row.createCell(0);
				cell.setCellValue("Client"); 
				cell.setCellStyle(set_style);
				firstSheet.setColumnWidth(0, 10000);
			}
			
			if ( lastrow1 == 0 ) 
			{
				cell1 = row.createCell(row.getLastCellNum());
			}
			else 
			{
				row.removeCell(row.getCell(row.getLastCellNum() - 1));//added on 11-09-2017 because total attribute in column got shifted by 1 place in line no 261
				row.removeCell(row.getCell(row.getLastCellNum() - 1));// added this code today on 26-10-2017
				cell1 = row.createCell(row.getLastCellNum());// here -1 was present, later removed because of above comment 
			}
			
			int curr_year3;
			
			SimpleDateFormat mon = new SimpleDateFormat("MMM");
			SimpleDateFormat yy = new SimpleDateFormat("YY");
	  
			String currentmonth = mon.format(new Date());
			String currentyear = yy.format(new Date());
			
			// added this code for getting year depending on month
			int curr_year2 = Integer.parseInt(currentyear);
		
			  if ( currentmonth.equals("Jan") ) //Need to change month, to Jan
			  {
				  curr_year3 = curr_year2 - 1;
		      }
			  else if ( currentmonth.equals("Feb") ) //Need to change month, to Feb
			 {
				  curr_year3 = curr_year2; //Need to remove +1.
				  System.out.println(curr_year3);
		     }
			  else 
			 {
			      curr_year3 = curr_year2;
			 }
			 // end
			 
			 SimpleDateFormat sdf = new SimpleDateFormat("MMM");
			 Calendar c1 = Calendar.getInstance();
			 c1.add(Calendar.MONTH,-1);
			 System.out.println(sdf.format(c1.getTime()));
			 String prv_month = sdf.format(c1.getTime());
	  
			String curr_mon_date = prv_month + "-" + curr_year3;//removed currentyear variable since it was always giving current year. we need to change it depending on month.
		
			//System.out.println(row.getLastCellNum());
			cell1.setCellValue(curr_mon_date);
			cell1.setCellStyle(set_style);	
			
			// added this code today on 26-10-2017
			Cell cell20 = row.getCell(row.getLastCellNum());
			if ( cell20 == null ) {
				cell20 = row.createCell(row.getLastCellNum());
				cell20.setCellStyle(set_style5);
			}
			// end here removed +1 from below
			// added this code today on 26-10-2017
			int lastrow20 = firstSheet.getLastRowNum();
			Cell cell8 = null;
			if ( lastrow20 == 6 ) {
			cell8 = row.createCell(row.getLastCellNum());//here + 1 was added later to shift the total attribute by 1 place in column because of line 261
			cell8.setCellValue("Total");
			//cell8.setCellStyle(set_style);
			cell8.setCellStyle(set_style5);
			}
			else {
				cell8 = row.createCell(row.getLastCellNum());//here + 1 was added later to shift the total attribute by 1 place in column because of line 261
				cell8.setCellValue("Total");
				cell8.setCellStyle(set_style5);} // added this code today on 26-10-2017 --ended here
			//cell8.setCellStyle(border_style);
			//cell8.setCellStyle(set_style);
		
		
	  int nexttrow = 1;
	  
	  int lastrow = firstSheet.getLastRowNum();
	  
	  if ( lastrow == 6 ) //CHANGED FROM 0 TO 6
	  {
		  int row_create = 8;//CHANGED FROM 1 TO 7 , later changed from 7 to 8 for adding one blank row below client
		  Row row2 = null;
		  for (int j = 0; j < l1.size(); j++) {
		  
		  List<String> l3 = l1.get(j);
		  int p = 1;
		  System.out.println(l3);

		  for(int k=0; k<l3.size(); k++)
        {
			int cell_no = 0;
			if ( p == 1) {
			row2 = firstSheet.createRow(row_create);
			Cell cell2 = row2.createCell(0);
			cell2.setCellValue(String.valueOf(l3.get(0))); 
			p = 0; }
			else {
			cell_no = row2.getLastCellNum();
			
			Cell cell4 = row2.createCell(cell_no);
			cell4.setCellValue(String.valueOf(l3.get(1)));  }
		}
		row_create++;
		  }
	  }
		
		else 
		{
			firstSheet.removeRow(firstSheet.getRow(lastrow));
			
			firstSheet.removeRow(firstSheet.getRow(1));
			firstSheet.removeRow(firstSheet.getRow(2));
			firstSheet.removeRow(firstSheet.getRow(3));
			
			int rowcount = 8;//CHANGED FROM 1 TO 7, later changed from 7 to 8 for adding one blank row below client
			for (int j = 0; j < l1.size(); j++) {
		  
			List<String> l4 = l1.get(j);
			
			for(int k=1; k<l4.size(); k++)
			{
				//System.out.println(" >> "+workbook.getSheetAt(0).getRow(rowcount));
				Row str3 = workbook.getSheetAt(0).getRow(rowcount);
				if ( str3 != null)  {
				Cell cell_no1 = workbook.getSheetAt(0).getRow(rowcount).getCell(0);
				
				String cell_value_excel = cell_no1.getStringCellValue();
				
				String list_cell_value = String.valueOf(l4.get(0));
				
				if ( cell_value_excel.equals(list_cell_value) ) 
				{
					Row row5 = firstSheet.getRow(rowcount);
					row5.removeCell(row5.getCell(row5.getLastCellNum() - 1));//added this code on 11-09-2017 for removing data in total attribute row by row, because of line 261
				Cell cell5 = row5.createCell(row5.getLastCellNum());//no value was present here,then added -1,and again removed for shifting total attribute in line 261
					//cell5.setCellValue(String.valueOf(0));//added extra code on 11-09 for resetting total value
					cell5.setCellValue(String.valueOf(l4.get(1)));
				} }
				else 
				{
					Row row6 = firstSheet.createRow(rowcount);
					Cell cell6 = row6.createCell(0);
					cell6.setCellValue(String.valueOf(l4.get(0)));
					
					Cell cell7 = row6.createCell(row.getLastCellNum()-3);//-2 was present, later replaced by -3 for shifting total attribute by 1 row in line no 261
					cell7.setCellValue(String.valueOf(l4.get(1)));
				}
			}
			rowcount++;
			}
		}
	  
	  //inputStream.close();
	  
	  //FileOutputStream outputStream = new FileOutputStream("ExcelSheet.xls");
	  FileOutputStream outputStream = new FileOutputStream(final_excel_path);
		
      workbook.write(outputStream);
      //workbook.close();
      outputStream.close();
	  
    } catch (Exception e) {
      e.printStackTrace();
	  
    }
  }
}


//Code for reading data from excel and writing total sum
class ReadPrintTotal {
  int rownum = 0;
  //HSSFSheet firstSheet;
  Collection<File> files;
  //HSSFWorkbook workbook;
  File exactFile;
  
  //String excelFilePath1 = "ExcelSheet.xls";
  
  /**{
    workbook = new HSSFWorkbook();
    firstSheet = workbook.createSheet("Client Cases Count");
    //Row headerRow = firstSheet.createRow(rownum);
    //headerRow.setHeightInPoints(40);
  }*/
  
 public void ReadWriteTotal(String final_excel_path) throws Exception {
    try {
		
		FileInputStream inputStream1 = new FileInputStream(final_excel_path);
		Workbook workbook1 = WorkbookFactory.create(inputStream1);
		
		Sheet ReadSheet = workbook1.getSheetAt(0);
        Iterator<Row> rowIter1 = ReadSheet.rowIterator();
		
		CellStyle set_style6 = workbook1.createCellStyle();//added this code for giving colours to text
		Font set_font_color6 = workbook1.createFont();//added this code for giving colours to text
		set_font_color6.setColor(IndexedColors.BLUE.getIndex());//added this code for giving colours to text
		set_style6.setFont(set_font_color6);//added this code for giving colours to text

		int i = 0;
		int row_inc = 8;  //changed from 7 to 8 for adding one blank row below client
		while (rowIter1.hasNext()) 
		{
			Row currentRow1 = rowIter1.next();
			//System.out.println(currentRow1);
			
			Iterator<Cell> cellIterator = currentRow1.cellIterator();;
				
				int column_total1 = 0;
				int g = 0;
				String str5 = null;
				
				if ( i ==1 ) {

			while (cellIterator.hasNext()) {
					
					Cell currentCell1 = cellIterator.next();
					//System.out.println(currentCell1.getCellType());
			          switch (currentCell1.getCellType()) 
					  {
						  case 1:
						  if ( str5 == null ) {
						  str5 = currentCell1.getStringCellValue();}
						  
						  
						  if ( g == 1 ) {
						 String str4 = currentCell1.getStringCellValue();
						 //System.out.println(str4);
						 int column_total = Integer.parseInt(str4);
						 
						 //System.out.println(column_total1);
						 column_total1 = column_total1 + column_total;
					     
						  }
						  
						  break;
					  }  
					  g = 1;
				}  
				
				Row row9 = ReadSheet.getRow(row_inc);
				Cell cell9 = row9.createCell(row9.getLastCellNum() + 1);//no value was present, added +1 because total attribute in column got shifted by 1 place in line 261
				//System.out.println(column_total1);
				cell9.setCellValue(String.valueOf(column_total1));
				cell9.setCellStyle(set_style6);
				row_inc++;
				} 
				i = 1; 
		}
		
		inputStream1.close();
	  
	  FileOutputStream outputStream1 = new FileOutputStream(final_excel_path);
		
      workbook1.write(outputStream1);
      //workbook.close();
      outputStream1.close();
		
	} catch (Exception e) {
      e.printStackTrace();
	}
  }
}

//code for printing total column sum in row

class ReadPrintColumnTotal {
  int rownum = 0;
  //HSSFSheet firstSheet;
  Collection<File> files;
  //HSSFWorkbook workbook;
  File exactFile;
  
  //String excelFilePath2 = "ExcelSheet.xls";
  
  /**{
    workbook = new HSSFWorkbook();
    firstSheet = workbook.createSheet("Client Cases Count");
    //Row headerRow = firstSheet.createRow(rownum);
    //headerRow.setHeightInPoints(40);
  }*/
  
 public int ReadWriteColumnTotal(String final_excel_path) throws Exception {
    try {
		
		FileInputStream inputStream2 = new FileInputStream(final_excel_path);
		Workbook workbook2 = WorkbookFactory.create(inputStream2);
		Sheet ReadSheet2 = workbook2.getSheetAt(0);
		
		Row row10 = ReadSheet2.getRow(6);
		int cell10 = row10.getLastCellNum() - 3; 
		
		Row row11 = ReadSheet2.createRow(ReadSheet2.getLastRowNum()+2);

		int l = 0;
		int column_total4 = 0;
		int column_total5 = 0;
		
		Cell cell11 = row11.createCell(l);
		
		CellStyle set_style1 = workbook2.createCellStyle();
		Font set_font1 = workbook2.createFont();
		set_font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
		set_style1.setFont(set_font1);
		
		CellStyle set_style7 = workbook2.createCellStyle();//added this code for giving colours to text
		Font set_font_color7 = workbook2.createFont();//added this code for giving colours to text
		set_font_color7.setBoldweight(Font.BOLDWEIGHT_BOLD);//added this code for giving colours to text
		set_font_color7.setColor(IndexedColors.BLUE.getIndex());//added this code for giving colours to text
		set_style7.setFont(set_font_color7);//added this code for giving colours to text
		
		CellStyle set_style8 = workbook2.createCellStyle();//added this code for giving colours to text
		Font set_font_color8 = workbook2.createFont();//added this code for giving colours to text
		set_font_color8.setColor(IndexedColors.BLUE.getIndex());//added this code for giving colours to text
		set_style8.setFont(set_font_color8);//added this code for giving colours to text
		
		cell11.setCellValue("Total");
		//cell11.setCellStyle(set_style1);
		cell11.setCellStyle(set_style7);
		
			for (int r = 0; r <= cell10; r++)
			{	
		       int e = 0;
	
		   Sheet ReadSheet3 = workbook2.getSheetAt(0);
		   Iterator<Row> rowIter2 = ReadSheet3.rowIterator();
		   
		   int column_total2 = 0;
		
		if ( l == 1 ) 
		{
		  while (rowIter2.hasNext()) 
		    {
				Row currentRow2 = rowIter2.next();
			
				if ( e == 1 )
				{
					String str7 = null;
					//Row currentRow2 = rowIter2.next();	
					Cell currentCell2 = currentRow2.getCell(r);
					System.out.println(currentCell2);
					
					if ( currentCell2 != null )
					{
					str7 = currentCell2.getStringCellValue();
					System.out.println("column total :" +str7);
					
					int column_total3 = Integer.parseInt(str7);
				    column_total2 = column_total2 + column_total3;
					}
				}
				e = 1;
			} 
			 Cell cell12 = row11.createCell(r); 
			 cell12.setCellValue(String.valueOf(column_total2));
			 cell12.setCellStyle(set_style8);
		} 
		l = 1; 
		
		column_total4 = column_total4 + column_total2;
		column_total5 = column_total2;
	}
	
	Cell cell13 = row11.createCell(row11.getLastCellNum() + 1); 
	cell13.setCellValue(String.valueOf(column_total4));
	cell13.setCellStyle(set_style8);
	
	CellStyle set_style2 = workbook2.createCellStyle();
	Font set_font2 = workbook2.createFont();
	set_font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
	set_style2.setFont(set_font2);
	
	Row row13 = ReadSheet2.createRow(1);
	Cell cell14 = row13.createCell(0);
	cell14.setCellValue("CRF Consumption for all the Clients");
	cell14.setCellStyle(set_style2);

	Row row14 = ReadSheet2.createRow(2);
	Cell cell15 = row14.createCell(0);
	cell15.setCellValue("Total Purchased 50,000");
	cell15.setCellStyle(set_style2);
	
	Row row15 = ReadSheet2.createRow(3);
	Cell cell16 = row15.createCell(0);
	cell16.setCellValue("Projection by Ops - ");
	cell16.setCellStyle(set_style2);
		  
		  inputStream2.close();
	  
		  FileOutputStream outputStream2 = new FileOutputStream(final_excel_path);
		
		  workbook2.write(outputStream2);
	
		  outputStream2.close();
		  
		  return column_total5;
		  
	  } catch (Exception e) {
      e.printStackTrace();
	}
	return -1;
	
  }
}

// start of code for sending mail

class SendEmail {

   public void SendMailFile(String final_excel_path, int a2) throws Exception {    

      String to = "anselm.nogueira@sciformix.com, sipdipti.rane@sciformix.com, isha.sharma@sciformix.com, sunit.singh@sciformix.com";
      String from = "ithelpdesk@sciformix.com";
      String host = "115.117.50.1";
	  
      Properties properties = System.getProperties();
      properties.setProperty("mail.smtp.host", host);

      // Get the default Session object.
      Session session = Session.getDefaultInstance(properties);
	  
	  SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM");
	  Calendar c2 = Calendar.getInstance();
	  c2.add(Calendar.MONTH,-1);
	  //System.out.println(sdf.format(c1.getTime()));
	  String month = sdf1.format(c2.getTime());
	

      try {
		 
		 //setVisible(true);		 
         // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(session);
		 
		MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
		String filename = final_excel_path;//"Details.xlsx";//change accordingly  
		
		String message_text = "Total case count for the month of " + month;
		String message_text1 = " : " + a2;
		String message_body_text1 = message_text + message_text1 + "\r \r \r \r \r \r \r \r \r \r \n Thanks and Regards \r IT Help Desk Team";
		String message_body_text2 = message_body_text1 + "\r \n This is an auto generated email.Please do not reply to this mail.";
		String message_body_text = message_body_text2 + "\r you are receiving this email as a part of select recipients who have privilege of knowing the amount of cases processed in Sciformix cloud.";
		
		DataSource source = new FileDataSource(filename);  
		messageBodyPart2.setDataHandler(new DataHandler(source));  
		messageBodyPart2.setFileName(filename.substring(28));  
		
		BodyPart messageBodyPart1 = new MimeBodyPart();  
        messageBodyPart1.setText(message_body_text);
		
		
		Multipart multipart = new MimeMultipart();  
		multipart.addBodyPart(messageBodyPart1);  
		multipart.addBodyPart(messageBodyPart2);  
		
		message.setContent(multipart);  

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         //message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		 
		 String[] recipientList = to.split(",");
		 InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
		 int counter = 0;
		 for (String recipient : recipientList) 
		 {
			recipientAddress[counter] = new InternetAddress(recipient.trim());
			counter++;
		 }
			
		message.setRecipients(Message.RecipientType.TO, recipientAddress);

         // Set Subject: header field
         message.setSubject(month + ": Year to date Case Consumption Count");

         // Send message
         Transport.send(message);
         System.out.println("Sent message successfully....");
      }catch (Exception mex) {
         mex.printStackTrace();
      }
   }
}
// end of code for sending mail
